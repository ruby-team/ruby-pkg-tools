# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2006 Thierry Reding <thierry@doppeltgemoppelt.de>
# Description:
#

ifndef _cdbs_bootstrap
	_cdbs_scripts_path ?= /usr/lib/cdbs
	_cdbs_rules_path ?= /usr/share/cdbs/1/rules
	_cdbs_class_path ?= /usr/share/cdbs/1/class
endif

ifndef _cdbs_class_ruby-extconf-rb
_cdbs_class_ruby-extconf-rb := 1

include /usr/share/ruby-pkg-tools/1/class/ruby-common.mk

DEB_RUBY_SETUP_CMD = extconf.rb
DEB_RUBY_SETUP_ARGS =

cdbs_pkgdir = $(CURDIR)/debian/$(cdbs_curpkg)
cdbs_ruby_libdir_ver = $(cdbs_pkgdir)$(DEB_RUBY_LIBDIR)
cdbs_ruby_archdir_ver = $(cdbs_pkgdir)$(DEB_RUBY_ARCHDIR)

DEB_RUBY_INSTALL_ARGS = DESTDIR=$(cdbs_pkgdir) \
	sitelibdir=$(cdbs_ruby_libdir_ver) \
        sitearchdir=$(cdbs_ruby_archdir_ver)

# Build simple packages.
$(patsubst %,build/%,$(DEB_RUBY_SIMPLE_PACKAGES)) :: build/% :
	cd $(DEB_SRCDIR)
	/usr/bin/ruby1.8 $(DEB_RUBY_SETUP_RUBY_ARGS) $(DEB_RUBY_SETUP_CMD) $(DEB_RUBY_SETUP_ARGS)
	$(MAKE)

# Install simple packages.
$(patsubst %,install/%,$(DEB_RUBY_SIMPLE_PACKAGES)) :: install/% :
	cd $(DEB_SRCDIR)
	$(MAKE) install $(DEB_RUBY_INSTALL_ARGS)

# Install regular library packages.
$(patsubst %,install/%,$(DEB_RUBY_REAL_LIB_PACKAGES)) :: install/% :
	cd $(DEB_SRCDIR) 
	[ ! -f Makefile ] || $(MAKE) distclean
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_RUBY_ARGS) $(DEB_RUBY_SETUP_CMD) $(DEB_RUBY_SETUP_ARGS)
	$(MAKE)
	$(MAKE) install $(DEB_RUBY_INSTALL_ARGS)

clean::
	[ ! -f Makefile ] || $(MAKE) distclean

endif

